MKDIR=mkdir -p
MOVE=mv
PKG_NAME=libcurl-test

all: build

build: mkdir lo_server test_curl

mkdir:
	$(MKDIR) $(DESTDIR)/usr/share/examples/$(PKG_NAME)

lo_server: local-server.o
	$(CC) local-server.o -o $@

local-server.o:
	$(CC) -c local-server.c

test_curl: curl-post.o
	$(CC) $(LDFLAGS) curl-post.o -o $@

curl-post.o:
	$(CC) $(CFLAGS) -c curl-post.c

install:
	$(MOVE) test_curl $(DESTDIR)/usr/share/examples/$(PKG_NAME)/
	$(MOVE) lo_server $(DESTDIR)/usr/share/examples/$(PKG_NAME)/

clean:
	rm *.o  
