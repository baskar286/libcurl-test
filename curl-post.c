#include <time.h>
#include <stdio.h>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <curl/curl.h>

int main(void)
{
	CURL *curl;
	CURLcode res;
	char temp[10] = "\0";
	srand(time(NULL));
  
	/* In windows, this will init the winsock stuff */ 
	curl_global_init(CURL_GLOBAL_ALL);
 
	/* get a curl handle */ 
	curl = curl_easy_init();
	if(curl) {
		/* First set the URL that is about to receive our POST. This URL can
		   just as well be a https:// URL if that is what should receive the
		   data. */ 
		curl_easy_setopt(curl, CURLOPT_URL, "http://localhost:15000");

		while (1) {
			if (read_tem(&temp))
				error(1, errno, "read error\n");
			/* Now specify the POST data */ 
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, temp);
 
			/* Perform the request, res will get the return code */ 
			res = curl_easy_perform(curl);
			/* Check for errors */ 
			if(res != CURLE_OK)
				fprintf(stderr, "curl_easy_perform() failed: %s\n",
						curl_easy_strerror(res));
		}
		/* always cleanup */ 
		curl_easy_cleanup(curl);
	}
	curl_global_cleanup();
	return 0;
}

int read_tem(char *value) {
	char *str[10] = {"34.45","35.00","33.33","32.20","29.90","30","31.30","31.23","30.0","32.3"};
	
	int r = rand() % 10;
	strcpy(value, str[r]);
	return 0;
}
